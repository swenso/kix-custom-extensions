var $tree;
document.addEventListener("DOMContentLoaded", function(event) {
 $element = $('#TextModules');
 if ($element.length > 0) {
  // console.log('Text Module found');
  // add custom search
  $('#TextModulesButtons', $element).append("<input type=\"text\" id=\"customSearch\" />");
  // TODO / WIP: add custom lang selection
  // $('#TextModulesButtons', $element).append("<select id=\"customLang\"><option value=\"all\">all</option><option value=\"de\">de</option><option value=\"fr\">fr</option><option value=\"en\">fr</option></select>");
  // toggle textbox
  $('.WidgetAction.Toggle a', $element).click();
  $tree = $(':jstree', $element);
  var treeData = $tree.jstree(true).get_json();
  //console.log('json', treeData);
  // remove old tree
  $tree.jstree().destroy();
  $('#TextModulesSelectionContainer').jstree({
   core: {
    animation: 70,
    data: treeData,
    dblclick_toggle: false,
    expand_selected_onload: true,
    themes: {
     name: 'InputField',
     variant: 'Tree',
     icons: true,
     dots: true,
    }
   },
   types: {
    default: {
     icon: 'fa fa-file-text-o'
    },
    category: {
     icon: 'fa fa-folder-open-o'
    },
   },
   search: {
    fuzzy: false,
    show_only_matches: true
   },
   plugins: ['types', 'search']
  });

  $("#customSearch").keyup(function() {

   var searchString = $(this).val();
   //console.log(searchString);
   $('#TextModulesSelectionContainer').jstree('search', searchString);
  });

 }
});